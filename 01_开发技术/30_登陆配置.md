## 登陆配置

<p class="show-images"><img src="/images/undraw_pair_programming_njlp.svg" width="40%" /></p>

登陆的一些配置，常用的验证码和错误次数，手机验证码或者邮箱验证码开启

### 登陆错误次数

yaml文件配置

```yaml
# 默认为5次
alinesno.login.max-retry: 5

# 错误账户锁住时间(分钟)，默认60分钟
alinesno.login.lock-time: 30
```

### 验证码开启

验证码开启在后台`系统参数`，设置`sys.captcha.status`即可，默认为开启验证码。

### 手机验证码开启

待补充

### 邮箱验证码开启

待补充

### 其它
- 略
